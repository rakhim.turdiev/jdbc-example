package com.example.vehicle;

import com.example.vehicle.controller.TravelController;
import com.example.vehicle.dto.TravelFinish;
import com.example.vehicle.dto.TravelStart;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class TravelTest {
    protected Travel travel;
    @Autowired
    private TravelController travelController;

    public void run(Vehicle vehicle, LocalDate date) {
        LocalTime time = LocalTime.now();
        travelStart(vehicle, "Toshkent - Samarqand", "Samarqandga sayohat", date, time);
        travelFinish(300, 20, date, time = time.plusHours(4));
        travelStart(vehicle, "Samarqand - Navoiy", "Navoiyga sayohat", date, time);
        travelFinish(200, 16, date, time = time.plusHours(2));
        travelStart(vehicle, "Navoiy - Buxoro", "Buxoroga sayohat", date, time);
        travelFinish(100, 8, date, time = time.plusHours(1));
        travelStart(vehicle, "Buxoro - Xorazm", "Xorazmga sayohat", date, time);
        travelFinish(500, 50, date, time.plusHours(6));
        travelStart(vehicle, "Xorazm - Toshkent", "Toshkentga qaytish", date, time);
        travelFinish(1100, 80, date = date.plusDays(1), time = time.plusHours(15));

        travel.setDescription("Xorazmdan Toshkentga qaytish");
        ResponseEntity<Travel> response = travelController.change(travel);
        assert response.getStatusCode() == HttpStatus.OK;
        assert "Xorazmdan Toshkentga qaytish".equals(response.getBody().getDescription());
    }

    private void travelStart(Vehicle vehicle, String route, String description, LocalDate date, LocalTime time) {
        TravelStart travelStart = new TravelStart(vehicle.getId(),
                route, description, date, time);
        ResponseEntity<Travel> response = travelController.start(travelStart);
        assert response.getStatusCode() == HttpStatus.OK;
        assert !response.getBody().isNew();
        travel = response.getBody();
    }

    private void travelFinish(Integer kilometer, Integer fuel, LocalDate date, LocalTime time) {
        TravelFinish travelFinish = new TravelFinish(travel.getId(), kilometer, fuel, date, time);

        ResponseEntity<Travel> response = travelController.finish(travelFinish);
        assert response.getStatusCode() == HttpStatus.OK;
        assert response.getBody().getOdometrFinish() - response.getBody().getOdometrStart() == kilometer;
        travel = response.getBody();
    }
}