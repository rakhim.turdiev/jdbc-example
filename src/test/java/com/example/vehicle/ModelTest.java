package com.example.vehicle;

import com.example.vehicle.controller.ModelController;
import com.example.vehicle.entity.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ModelTest {

    @Autowired
    private ModelController modelController;

    public Model create(String name) {
        ResponseEntity<Model> response = modelController.save(new Model(name));
        assert response.getStatusCode() == HttpStatus.OK;
        assert !response.getBody().isNew();
        Model model = response.getBody();
        return model;
    }
    public void update(Model model,String name){
        model.setName("Malibu");
        ResponseEntity<Model> response = modelController.save(model);
        assert response.getStatusCode() == HttpStatus.OK;
        assert "Malibu".equals(response.getBody().getName());
    }
}
