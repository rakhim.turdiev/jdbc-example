package com.example.vehicle;

import com.example.vehicle.controller.VehicleController;
import com.example.vehicle.entity.Model;
import com.example.vehicle.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class VehicleTest {
    @Autowired
    private VehicleController vehicleController;

    public Vehicle create(Model model, String colour, String owner, String regNumber) {
        Vehicle vehicle = new Vehicle();
        vehicle.setColour(colour);
        vehicle.setOwner(owner);
        vehicle.setModel_id(model.getId());
        vehicle.setReg_numb(regNumber);
        ResponseEntity<Vehicle> response = vehicleController.save(vehicle);
        assert response.getStatusCode() == HttpStatus.OK;
        assert !response.getBody().isNew();
        return vehicle = response.getBody();
    }
}