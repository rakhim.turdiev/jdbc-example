package com.example.vehicle;

import com.example.vehicle.controller.VehicleDailyReportController;
import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Model;
import com.example.vehicle.entity.Vehicle;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class VehicleApplicationTests {

    @Autowired
    private ModelTest modelTest;
    @Autowired
    private VehicleTest vehicleTest;
    @Autowired
    private TravelTest travelTest;
    @Autowired
    private VehicleDailyReportController vehicleDailyReportController;
    private static Model malibu;
    private static Model cobalt;
    private static Vehicle whiteMalibu;
    private static Vehicle blackCobalt;

    @Order(value = 1)
    @Test
    void model() {
        cobalt = modelTest.create("Cobalt");
        malibu = modelTest.create("malibu");
        modelTest.update(malibu, "Malibu");
    }

    @Order(value = 2)
    @Test
    void vehicle() {
        whiteMalibu = vehicleTest.create(malibu, "White", "Ergashev", "01 A 345 DF");
        blackCobalt = vehicleTest.create(malibu, "black", "Husanov", "01 A 346 DF");
    }

    @Order(value = 3)
    @Test
    void travel() {
        travelTest.run(whiteMalibu, LocalDate.now().minusDays(10));
        travelTest.run(whiteMalibu, LocalDate.now().minusDays(8));
        travelTest.run(whiteMalibu, LocalDate.now().minusDays(6));
        travelTest.run(whiteMalibu, LocalDate.now().minusDays(4));
        travelTest.run(whiteMalibu, LocalDate.now().minusDays(2));
        travelTest.run(whiteMalibu, LocalDate.now());
        travelTest.run(blackCobalt, LocalDate.now().minusDays(10));
        travelTest.run(blackCobalt, LocalDate.now().minusDays(8));
        travelTest.run(blackCobalt, LocalDate.now().minusDays(6));
        travelTest.run(blackCobalt, LocalDate.now().minusDays(4));
        travelTest.run(blackCobalt, LocalDate.now().minusDays(2));
        travelTest.run(blackCobalt, LocalDate.now());
    }

    @Order(4)
    @Test
    void report() {
        ResponseEntity<ListResult> response = vehicleDailyReportController.list(whiteMalibu.getId(), LocalDate.now(), null, null);
        assert response.getStatusCode() == HttpStatus.OK;
        assert response.getBody().getRows().size() == 1;

        response = vehicleDailyReportController.list(null, LocalDate.now(), null, null);
        assert response.getStatusCode() == HttpStatus.OK;
        assert response.getBody().getRows().size() == 2;
    }
}