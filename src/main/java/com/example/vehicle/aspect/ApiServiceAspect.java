package com.example.vehicle.aspect;

import com.example.vehicle.dto.TravelFinish;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.service.VehicleDailyReportService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ApiServiceAspect {
    @Autowired
    private VehicleDailyReportService vehicleDailyReportService;

    @Pointcut("execution(public * com.example.vehicle.controller.TravelController.finish(..))")
    public void methods() {
    }

    @Before("methods()")//applying pointcut on before advice
    public void before(JoinPoint jp)//it is advice (before advice)
    {
        TravelFinish travelFinish = (TravelFinish) jp.getArgs()[0];
    }

    @After("methods()")//applying pointcut on before advice
    public void after(JoinPoint jp)//it is advice (before advice)
    {
        try {
            Travel travel = (Travel) ((ResponseEntity) ((MethodInvocationProceedingJoinPoint) jp).proceed()).getBody();
            vehicleDailyReportService.update(travel);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
