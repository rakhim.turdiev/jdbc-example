package com.example.vehicle.dao;

import com.example.vehicle.entity.Entity;
import com.example.vehicle.filter.FilterParameters;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

@ConfigurationProperties(
        prefix = "spring.db"
)
public abstract class DbManager<T extends Entity> {

    public abstract void setQueryBuilder(String builderClass) throws ClassNotFoundException, InstantiationException, IllegalAccessException;

    public abstract T add(T entity);

    public abstract List list(FilterParameters filterParameters);

    public abstract Object getObject(FilterParameters filterParameters);

    public abstract Boolean delete(Class<T> delete, Long id);

    public abstract Map<String, Object> get(FilterParameters filterParameters);

    public abstract T update(T travel);
}