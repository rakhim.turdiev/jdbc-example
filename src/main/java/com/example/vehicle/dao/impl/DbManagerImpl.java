package com.example.vehicle.dao.impl;

import com.example.vehicle.config.Table;
import com.example.vehicle.dao.DbManager;
import com.example.vehicle.dao.QueryBuilder;
import com.example.vehicle.entity.Entity;
import com.example.vehicle.filter.FilterParameters;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

import static com.example.vehicle.utils.Utils.camelToSnake;

@Component
public class DbManagerImpl<T extends Entity> extends DbManager<T> {
    @Value(value = "${spring.datasource.url}")
    private String dbUrl;
    @Value(value = "${spring.datasource.username}")
    private String user;
    @Value(value = "${spring.datasource.password}")
    private String password;
    private Connection connection;

    private QueryBuilder builder;

    @PostConstruct
    public void init() {
        try {
            connection = DriverManager.getConnection(dbUrl, user, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public T add(T entity) {
        if (entity == null)
            throw new RuntimeException("Table not found");

        //table name
        String tableName = camelToSnake(entity.getClass().getSimpleName());

        Table table = entity.getClass().getDeclaredAnnotation(Table.class);
        if (table != null && !ObjectUtils.isEmpty(table.name()))
            tableName = table.name();


        //get parameters
        List<String> fields = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        getParameters(entity, fields, values);

        String insertCode = builder.insertGenerateCode(tableName, fields);

        //Set parameters
        try {
            PreparedStatement statement = executeWithParameterts(insertCode, fields.size(), values);
            ResultSet result = statement.executeQuery();
            Long id = null;
            if (result.next())
                id = result.getLong(1);
            statement.close();
            entity.setId(id);
            return entity;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List list(FilterParameters filterParameters) {
        try {
            String query = builder.listGenerateCode(filterParameters);

            //set parameter
            PreparedStatement statement = executeWithParameters(filterParameters, query);

            //Execute Sql
            ResultSet resultSet = statement.executeQuery();

            //Get column list
            ResultSetMetaData metaData = resultSet.getMetaData();
            List<String> columnNames = new ArrayList<>();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                columnNames.add(metaData.getColumnName(i));
            }

            //Get data
            List<Map<String, Object>> data = new LinkedList<>();
            while (resultSet.next()) {
                Map<String, Object> rowItem = new HashMap<>();
                for (int i = 0; i < metaData.getColumnCount(); i++) {
                    rowItem.put(columnNames.get(i), resultSet.getObject(i + 1));
                }
                data.add(rowItem);
            }
            resultSet.close();
            return data;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Object getObject(FilterParameters filterParameters) {
        try {
            String query = builder.getGenerateCode(filterParameters);

            PreparedStatement statement = executeWithParameters(filterParameters, query);

            //Execute Sql
            ResultSet resultSet = statement.executeQuery();

            //Get data
            if (resultSet.next()) {
                Object data = resultSet.getObject(1);
                resultSet.close();
                return data;
            } else
                return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Boolean delete(Class<T> clazz, Long id) {
        if (id == null)
            throw new RuntimeException("ID not found");

        //table name
        String tableName = camelToSnake(clazz.getSimpleName());

        Table table = clazz.getDeclaredAnnotation(Table.class);
        if (table != null && !ObjectUtils.isEmpty(table.name()))
            tableName = table.name();

        String deleteCode = builder.deleteGenerateCode(tableName);

        //Set parameters
        try {
            PreparedStatement statement = connection.prepareStatement(deleteCode);
            statement.setObject(1, id);

            int result = statement.executeUpdate();
            statement.close();
            return result > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, Object> get(FilterParameters filterParameters) {
        filterParameters.setLimit(1);
        List list = list(filterParameters);
        if (list.isEmpty())
            return null;
        else
            return (Map<String, Object>) list.get(0);
    }

    @Override
    public T update(T entity) {
        if (entity == null)
            throw new RuntimeException("ID not found");

        //table name
        String tableName = camelToSnake(entity.getClass().getSimpleName());

        Table table = entity.getClass().getDeclaredAnnotation(Table.class);
        if (table != null && !ObjectUtils.isEmpty(table.name()))
            tableName = table.name();
        //get parameters
        List<String> fields = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        getParameters(entity, fields, values);

        String updateCode = builder.updateGenerateCode(tableName, entity.getId(), fields);

        //Set parameters
        try {
            values.add(entity.getId());
            PreparedStatement statement = executeWithParameterts(updateCode, fields.size(), values);
            int result = statement.executeUpdate();
            statement.close();
            return entity;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private PreparedStatement executeWithParameterts(String query, int fields, List<Object> values) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);
        for (int i = 0; i < values.size(); i++) {
            statement.setObject(i + 1, values.get(i));
        }
        return statement;
    }

    private PreparedStatement executeWithParameters(FilterParameters filterParameters, String query) throws SQLException {
        //set parameter
        PreparedStatement statement = executeWithParameterts(query, filterParameters.getParameters().size(), filterParameters.getParameters());
        return statement;
    }

    private void getParameters(T entity, List<String> fields, List<Object> values) {
        if (entity == null)
            throw new RuntimeException("Entity is null");

        Field[] declaredFields = FieldUtils.getAllFields(entity.getClass());
        if (entity.isNew()) {
            //Need to insert
            declaredFields = Arrays.asList(declaredFields).stream()
                    .filter(field -> !"id".equals(field.getName()))
                    .toList()
                    .toArray(new Field[]{});
        }
        for (Field field : declaredFields) {
            try {
                field.setAccessible(true);
                Object value = field.get(entity);
                if (value != null) {
                    if (value.getClass().isEnum()) {
                        values.add(value.toString());
                    } else
                        values.add(value);
                    fields.add(camelToSnake(field.getName()));
                }
            } catch (IllegalAccessException e) {
            }
        }
    }


    public void setQueryBuilder(String builderClass) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        this.builder = (QueryBuilder) Class.forName(builderClass).newInstance();
    }
}
