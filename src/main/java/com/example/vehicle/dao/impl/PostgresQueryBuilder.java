package com.example.vehicle.dao.impl;

import com.example.vehicle.dao.QueryBuilder;
import com.example.vehicle.filter.FilterParameters;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Component
public class PostgresQueryBuilder implements QueryBuilder {
    @Override
    public String insertGenerateCode(String tableName, List fields) {
        //generate insert code
        StringBuilder insertCode = new StringBuilder(String.format("INSERT INTO %s (%s) select ",
                tableName,
                String.join(" , ", fields)));
        for (int i = 0; i < fields.size(); i++) {
            if (i > 0)
                insertCode.append(",");
            insertCode.append("?");
        }
        insertCode.append(" RETURNING id");
        return insertCode.toString();
    }

    @Override
    public String deleteGenerateCode(String tableName) {
        return String.format("UPDATE %s set state = 'DELETED' where id = ?", tableName);
    }

    @Override
    public String listGenerateCode(FilterParameters parameters) {
        //Generate native SQL code
        StringBuilder query = new StringBuilder();
        query.append("SELECT ").append(parameters.select())
                .append(" FROM ").append(parameters.from());

        String where = parameters.where();
        if (!ObjectUtils.isEmpty(where))
            query.append(" WHERE ").append(where);

        if (!ObjectUtils.isEmpty(parameters.order())) {
            query.append(" ORDER BY ").append(parameters.order());
        }
        if (!ObjectUtils.isEmpty(parameters.group())) {
            query.append(" GROUP BY ").append(parameters.group());
        }
        if (!ObjectUtils.isEmpty(parameters.getOffset())) {
            query.append(" OFFSET ").append(parameters.getOffset());
        }
        if (!ObjectUtils.isEmpty(parameters.getLimit())) {
            query.append(" LIMIT ").append(parameters.getLimit());
        }
        return query.toString();
    }

    @Override
    public String getGenerateCode(FilterParameters parameters) {
        //Generate native SQL code
        StringBuilder query = new StringBuilder();
        query.append("SELECT ").append(parameters.select())
                .append(" FROM ").append(parameters.from());

        String where = parameters.where();
        if (!ObjectUtils.isEmpty(where))
            query.append(" WHERE ").append(where);

        if (!ObjectUtils.isEmpty(parameters.order())) {
            query.append(" ORDER BY ").append(parameters.order());
        }
        if (!ObjectUtils.isEmpty(parameters.group())) {
            query.append(" GROUP BY ").append(parameters.group());
        }
        return query.toString();
    }

    @Override
    public String updateGenerateCode(String tableName, Long id, List<String> fields) {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE ").append(tableName).append(" SET ");
        for (int i = 0; i < fields.size(); i++) {
            String column = fields.get(i);
            if (i > 0)
                query.append(", ");
            query.append(column).append(" = ? ");
        }
        query.append(" WHERE id = ?");
        return query.toString();
    }
}