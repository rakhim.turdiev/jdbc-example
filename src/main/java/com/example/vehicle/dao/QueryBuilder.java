package com.example.vehicle.dao;

import com.example.vehicle.filter.FilterParameters;

import java.util.List;


public interface QueryBuilder {
    String insertGenerateCode(String tableName, List<String> fields);

    String deleteGenerateCode(String tableName);

    String listGenerateCode(FilterParameters builder);

    String getGenerateCode(FilterParameters filterParameters);

    String updateGenerateCode(String tableName, Long id, List<String> fields);
}