package com.example.vehicle.service;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Model;
import com.example.vehicle.filter.ModelFilter;

import java.util.List;
import java.util.Map;

public interface ModelService {
    Model save(Model model);

    ListResult list(ModelFilter filter);

    Boolean delete(Long id);

    List items(ModelFilter modelFilter);

    Map<String, Object> getById(Long id);
}
