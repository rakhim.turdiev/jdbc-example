package com.example.vehicle.service;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.dto.TravelFinish;
import com.example.vehicle.dto.TravelStart;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.filter.TravelFilter;

import java.util.Map;

public interface TravelService {
    Travel start(TravelStart start, Integer odometr);

    Travel save(Travel travel);

    ListResult list(TravelFilter travelFilter);

    Map<String, Object> getById(Long id);

    Boolean delete(Long id);

    Travel finish(TravelFinish travelFinish);
}