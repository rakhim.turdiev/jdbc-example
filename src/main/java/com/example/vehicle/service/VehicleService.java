package com.example.vehicle.service;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Vehicle;
import com.example.vehicle.filter.VehicleFilter;

import java.util.List;
import java.util.Map;

public interface VehicleService {
    ListResult list(VehicleFilter modelFilter);

    List items(VehicleFilter modelFilter);

    Map getById(Long id);

    boolean delete(Long id);

    Vehicle save(Vehicle model);

    void updateOdometer(Long vehicleId, Integer odometer);

    Integer getVehicleOdometer(Long vehicleId);
}
