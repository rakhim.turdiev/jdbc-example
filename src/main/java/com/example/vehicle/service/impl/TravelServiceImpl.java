package com.example.vehicle.service.impl;

import com.example.vehicle.dao.DbManager;
import com.example.vehicle.dto.ListResult;
import com.example.vehicle.dto.TravelFinish;
import com.example.vehicle.dto.TravelStart;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.filter.FilterParameters;
import com.example.vehicle.filter.TravelFilter;
import com.example.vehicle.service.TravelService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TravelServiceImpl implements TravelService {
    private final DbManager<Travel> dbManager;

    public TravelServiceImpl(DbManager<Travel> dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public Travel start(TravelStart travelStart, Integer odometerStart) {
        if (travelStart == null)
            throw new RuntimeException("Travel is null");
        Travel travel = travelStart.wrap();
        travel.setOdometrStart(odometerStart);
        save(travel);
        return travel;
    }

    @Override
    public Travel save(Travel travel) {
        if (travel == null)
            throw new RuntimeException("Travel is null");
        if (travel.isNew())
            return dbManager.add(travel);
        else
            return dbManager.update(travel);
    }

    @Override
    public ListResult list(TravelFilter travelFilter) {
        List list = getList(travelFilter);
        Long total = getTotal(travelFilter);
        return new ListResult(list, total);
    }

    @Override
    public Map<String, Object> getById(Long id) {
        return dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "t.id, v.colour,v.owner, v.reg_numb, t.odometr_start, t.odometr_finish, t.route, t.description,\n" +
                        " t.vehicle_id, t.start_date, t.start_time,t.end_date, t.end_time, m.name";
            }

            @Override
            public String from() {
                return "travel t join vehicle v on v.id = t.vehicle_id join model m on m.id = v.model_id";
            }

            @Override
            public String where() {
                addParam(id);
                return "t.state != 'DELETED' AND t.id = ?";
            }
        });
    }

    @Override
    public Boolean delete(Long id) {
        return dbManager.delete(Travel.class, id);
    }

    @Override
    public Travel finish(TravelFinish travelFinish) {
        if (travelFinish == null)
            throw new RuntimeException("Request empty");
        if (travelFinish.getTravelId() == null)
            throw new RuntimeException("Travel not found");

        Map<String, Object> travelMap = getById(travelFinish.getTravelId());
        Travel travel = new Travel();
        travel.setMap(travelMap);
        travel.setOdometrFinish(travel.getOdometrStart() + travelFinish.getKilometer());
        travel.setFuel(travelFinish.getFuel());
        travel.setEndDate(travelFinish.getDate());
        travel.setEndTime(travelFinish.getTime());
        save(travel);

        return travel;
    }

    private Long getTotal(TravelFilter travelFilter) {
        return (Long) dbManager.getObject(new FilterParameters() {
            @Override
            public String select() {
                return "count(*)";
            }

            @Override
            public String from() {
                return "travel t join vehicle v on v.id = t.vehicle_id join model m on m.id = v.model_id";
            }

            @Override
            public String where() {
                return filterQuery(travelFilter, this);
            }
        });
    }

    private List getList(TravelFilter travelFilter) {
        return dbManager.list(new FilterParameters() {
            @Override
            public String select() {
                return "t.id, v.colour,v.owner, v.reg_numb, t.odometr_start, t.odometr_finish, t.route, t.description,\n" +
                        "       t.start_date,t.start_time,t.end_date,t.end_time, m.name";
            }

            @Override
            public String from() {
                return "travel t join vehicle v on v.id = t.vehicle_id join model m on m.id = v.model_id";
            }

            @Override
            public String where() {
                return filterQuery(travelFilter, this);
            }
        });
    }

    private String filterQuery(TravelFilter travelFilter, FilterParameters filterParameters) {
        StringBuilder builder = new StringBuilder("t.state != 'DELETED' ");
        if (!ObjectUtils.isEmpty(travelFilter.getModel())) {
            filterParameters.addParam(travelFilter.getModel() + "%");
            builder.append(" AND m.name like ?");
        }
        if (!ObjectUtils.isEmpty(travelFilter.getOwner())) {
            filterParameters.addParam(travelFilter.getOwner() + "%");
            builder.append(" AND v.owner like ?");
        }
        if (!ObjectUtils.isEmpty(travelFilter.getReg_numb())) {
            filterParameters.addParam(travelFilter.getReg_numb() + "%");
            builder.append(" AND v.reg_number like ?");
        }
        if (!ObjectUtils.isEmpty(travelFilter.getFrom())) {
            filterParameters.addParam(new Date(travelFilter.getFrom().toEpochDay()));
            builder.append(" AND v.date >= ?");
        }
        if (!ObjectUtils.isEmpty(travelFilter.getTo())) {
            filterParameters.addParam(new Date(travelFilter.getTo().toEpochDay()));
            builder.append(" AND v.date <= ?");
        }
        return builder.toString();
    }
}