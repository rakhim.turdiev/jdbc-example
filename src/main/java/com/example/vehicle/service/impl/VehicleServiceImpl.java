package com.example.vehicle.service.impl;

import com.example.vehicle.dao.DbManager;
import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Vehicle;
import com.example.vehicle.filter.FilterParameters;
import com.example.vehicle.filter.VehicleFilter;
import com.example.vehicle.service.VehicleService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

@Service
public class VehicleServiceImpl implements VehicleService {
    private final DbManager<Vehicle> dbManager;

    public VehicleServiceImpl(DbManager<Vehicle> dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public ListResult list(VehicleFilter modelFilter) {
        List list = items(modelFilter);
        Long total = (Long) dbManager.getObject(new FilterParameters() {
            @Override
            public String select() {
                return "count(*)";
            }

            @Override
            public String from() {
                return "vehicle v left join model m on v.model_id = m.id";
            }

            @Override
            public String where() {
                StringBuilder builder = new StringBuilder("v.state != 'DELETED' ");
                if (!ObjectUtils.isEmpty(modelFilter.getModel())) {
                    addParam(modelFilter.getModel() + "%");
                    builder.append(" AND m.name like ?");
                }
                if (!ObjectUtils.isEmpty(modelFilter.getOwner())) {
                    addParam(modelFilter.getOwner() + "%");
                    builder.append(" AND v.owner like ?");
                }
                if (!ObjectUtils.isEmpty(modelFilter.getReg_numb())) {
                    addParam(modelFilter.getReg_numb() + "%");
                    builder.append(" AND v.reg_number like ?");
                }
                return builder.toString();
            }
        });
        return new ListResult(list, total);
    }

    @Override
    public List items(VehicleFilter modelFilter) {
        return dbManager.list(new FilterParameters(modelFilter.getPage(), modelFilter.getLimit()) {
            @Override
            public String select() {
                return "v.*, m.name model";
            }

            @Override
            public String from() {
                return "vehicle v left join model m on v.model_id = m.id";
            }

            @Override
            public String where() {
                StringBuilder builder = new StringBuilder("v.state != 'DELETED' ");
                if (!ObjectUtils.isEmpty(modelFilter.getModel())) {
                    addParam(modelFilter.getModel() + "%");
                    builder.append(" AND m.name like ?");
                }
                if (!ObjectUtils.isEmpty(modelFilter.getOwner())) {
                    addParam(modelFilter.getOwner() + "%");
                    builder.append(" AND v.owner like ?");
                }
                if (!ObjectUtils.isEmpty(modelFilter.getReg_numb())) {
                    addParam(modelFilter.getReg_numb() + "%");
                    builder.append(" AND v.reg_number like ?");
                }
                return builder.toString();
            }
        });
    }

    @Override
    public Map getById(Long id) {
        return dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "v.*, m.name model";
            }

            @Override
            public String from() {
                return "vehicle v left join model m on v.model_id = m.id";
            }

            @Override
            public String where() {
                if (id != null) {
                    addParam(id);
                }
                return "v.state !='DELETED' AND v.id = ?";
            }
        });
    }

    @Override
    public boolean delete(Long id) {
        return dbManager.delete(Vehicle.class, id);
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        if (vehicle == null)
            throw new RuntimeException("Vehicle is null");
        if (vehicle.isNew())
            return dbManager.add(vehicle);
        else
            return dbManager.update(vehicle);
    }

    @Override
    public void updateOdometer(Long vehicleId, Integer odometer) {
        Map<String, Object> vehicleMap = getById(vehicleId);
        Vehicle vehicle = new Vehicle();
        vehicle.setMap(vehicleMap);
        vehicle.setOdometr(odometer);
        save(vehicle);
    }

    @Override
    public Integer getVehicleOdometer(Long vehicleId) {
        Map vehile = getById(vehicleId);
        if (vehile == null)
            throw new RuntimeException("Vehicle not found");
        Integer odometr = (Integer) vehile.get("odometr");
        return odometr;
    }
}