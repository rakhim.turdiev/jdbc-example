package com.example.vehicle.service.impl;

import com.example.vehicle.dao.DbManager;
import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.entity.VehicleDailyReport;
import com.example.vehicle.filter.FilterParameters;
import com.example.vehicle.service.VehicleDailyReportService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@Service
public class VehicleDailyReportServiceImpl implements VehicleDailyReportService {
    private final DbManager<VehicleDailyReport> dbManager;

    public VehicleDailyReportServiceImpl(DbManager<VehicleDailyReport> dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public void update(Travel travel) {
        Map<String, Object> dailyTravel = getDailyTravel(travel.getVehicleId());
        Map<String, Object> daily = getExist(travel.getVehicleId(), travel.getEndDate());

        if (!ObjectUtils.isEmpty(dailyTravel)) {
            VehicleDailyReport dailyReport = new VehicleDailyReport(travel.getVehicleId(), travel.getEndDate());
            update(dailyTravel, daily, dailyReport);
        }

        //yesterday started and today finished
        if (travel.getStartDate() != travel.getEndDate()) {
            dailyTravel = getLastDayTravel(travel.getVehicleId());
            daily = getExist(travel.getVehicleId(), travel.getStartDate());

            if (!ObjectUtils.isEmpty(dailyTravel)) {
                VehicleDailyReport dailyReport = new VehicleDailyReport(travel.getVehicleId(), travel.getStartDate());
                update(dailyTravel, daily, dailyReport);
            }
        }
    }

    @Override
    public ListResult list(Long vehicleId, LocalDate date, Integer page, Integer limit) {
        List list = getList(vehicleId, date, page, limit);
        Long total = getTotal(vehicleId, date);

        return new ListResult(list, total);
    }

    private Long getTotal(Long vehicleId, LocalDate date) {
        return (Long) dbManager.getObject(new FilterParameters() {
            @Override
            public String select() {
                return "count(*)";
            }

            @Override
            public String from() {
                return "vehicle_daily_report";
            }

            @Override
            public String where() {
                StringBuilder builder = new StringBuilder("state != 'DELETED'");
                if (!ObjectUtils.isEmpty(vehicleId)) {
                    addParam(vehicleId);
                    builder.append(" AND vehicle_id = ?");
                }
                if (!ObjectUtils.isEmpty(date)) {
                    addParam(date);
                    builder.append(" AND date = ?");
                }
                return builder.toString();
            }
        });
    }

    private List getList(Long vehicleId, LocalDate date, Integer page, Integer limit) {
        return dbManager.list(new FilterParameters() {
            @Override
            public String select() {
                return "*";
            }

            @Override
            public String from() {
                return "vehicle_daily_report";
            }

            @Override
            public String where() {
                StringBuilder builder = new StringBuilder("state != 'DELETED'");
                if (!ObjectUtils.isEmpty(vehicleId)) {
                    addParam(vehicleId);
                    builder.append(" AND vehicle_id = ?");
                }
                if (!ObjectUtils.isEmpty(date)) {
                    addParam(date);
                    builder.append(" AND date = ?");
                }
                return builder.toString();
            }
        });
    }

    private void update(Map<String, Object> dailyTravel, Map<String, Object> daily, VehicleDailyReport dailyReport) {
        if (!ObjectUtils.isEmpty(daily)) {
            dailyReport.setId((Long) daily.get("id"));
        }
        dailyReport.setKilometer(((Double) dailyTravel.get("kilometer")).intValue());
        dailyReport.setTravel_count(((Long) dailyTravel.get("travel_count")).intValue());
        dailyReport.setDuring_time(LocalTime.parse("" + dailyTravel.get("during_time")));
        dailyReport.setFuel(((Double) dailyTravel.get("fuel")).intValue());
        dailyReport.setAvgSpeed(dailyReport.getKilometer() / (dailyReport.getDuring_time().getHour() + dailyReport.getDuring_time().getMinute() / 60));
        if (dailyReport.isNew())
            dbManager.add(dailyReport);
        else
            dbManager.update(dailyReport);
    }

    private Map<String, Object> getExist(Long vehicleId, LocalDate date) {
        Map<String, Object> daily = dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "*";
            }

            @Override
            public String from() {
                return "vehicle_daily_report";
            }

            @Override
            public String where() {
                addParam(vehicleId);
                addParam(date);
                return "vehicle_id = ? and date = ?";
            }
        });
        return daily;
    }

    private Map<String, Object> getDailyTravel(Long vehicleId) {
        Map<String, Object> dailyTravel = dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "*";
            }

            @Override
            public String from() {
                return "dailt_travel_report";
            }

            @Override
            public String where() {
                addParam(vehicleId);
                return "vehicle_id = ?";
            }
        });
        return dailyTravel;
    }

    private Map<String, Object> getLastDayTravel(Long vehicleId) {
        Map<String, Object> dailyTravel = dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "*";
            }

            @Override
            public String from() {
                return "last_day_travel_report";
            }

            @Override
            public String where() {
                addParam(vehicleId);
                return "vehicle_id = ?";
            }
        });
        return dailyTravel;
    }
}