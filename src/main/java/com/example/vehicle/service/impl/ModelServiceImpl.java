package com.example.vehicle.service.impl;

import com.example.vehicle.dao.DbManager;
import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Model;
import com.example.vehicle.filter.FilterParameters;
import com.example.vehicle.filter.ModelFilter;
import com.example.vehicle.service.ModelService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

@Service
public class ModelServiceImpl implements ModelService {
    private final DbManager<Model> dbManager;

    public ModelServiceImpl(DbManager<Model> dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public Model save(Model model) {
        if (model == null)
            throw new RuntimeException("Model not found");
        if (model.isNew())
            return dbManager.add(model);
        else
            return dbManager.update(model);
    }

    @Override
    public ListResult list(ModelFilter modelFilter) {
        List list = items(modelFilter);
        Long total = (Long) dbManager.getObject(new FilterParameters() {
            @Override
            public String select() {
                return "count(*)";
            }

            @Override
            public String from() {
                return "model";
            }

            @Override
            public String where() {
                if (!ObjectUtils.isEmpty(modelFilter.getName())) {
                    addParam(modelFilter.getName() + "%");
                    return "state != 'DELETED' AND name like ?";
                }
                return "";
            }
        });
        return new ListResult(list, total);
    }

    @Override
    public Boolean delete(Long id) {
        return dbManager.delete(Model.class, id);
    }

    @Override
    public List items(ModelFilter modelFilter) {
        return dbManager.list(new FilterParameters(modelFilter.getPage(), modelFilter.getLimit()) {
            @Override
            public String select() {
                return "id, name";
            }

            @Override
            public String from() {
                return "model";
            }

            @Override
            public String where() {
                if (!ObjectUtils.isEmpty(modelFilter.getName())) {
                    addParam(modelFilter.getName() + "%");
                    return "state != 'DELETED' AND name like ?";
                }
                return "";
            }

            @Override
            public String order() {
                return "id desc";
            }
        });
    }

    @Override
    public Map<String, Object> getById(Long id) {
        return dbManager.get(new FilterParameters() {
            @Override
            public String select() {
                return "id, name";
            }

            @Override
            public String from() {
                return "model";
            }

            @Override
            public String where() {
                if (!ObjectUtils.isEmpty(id)) {
                    addParam(id);
                    return "state = 'DELETED' AND id = ?";
                }
                return "";
            }
        });
    }
}