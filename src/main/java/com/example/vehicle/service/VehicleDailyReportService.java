package com.example.vehicle.service;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Travel;

import java.time.LocalDate;

public interface VehicleDailyReportService {

    void update(Travel travel);

    ListResult list(Long vehicleId, LocalDate date, Integer page, Integer limit);
}
