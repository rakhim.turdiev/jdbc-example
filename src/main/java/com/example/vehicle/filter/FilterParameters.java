package com.example.vehicle.filter;

import java.util.ArrayList;
import java.util.List;

public abstract class FilterParameters {
    private final List<Object> parameters = new ArrayList<>();
    protected Integer limit = 20;
    protected Integer offset = 0;

    public FilterParameters() {
    }

    public FilterParameters(Integer page, Integer limit) {
        if (limit != null)
            this.offset = (page - 1) * limit;
        if (limit != null)
            this.limit = limit;
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void addParam(Object obj) {
        this.parameters.add(obj);
    }

    public abstract String select();

    public abstract String from();

    public String where() {
        return "";
    }

    public String order() {
        return null;
    }

    public String group() {
        return null;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
