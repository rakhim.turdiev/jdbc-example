package com.example.vehicle.filter;

public class VehicleFilter extends FilterBase {
    private String reg_numb;
    private String owner;
    private String model;

    public VehicleFilter(Integer page, Integer limit, String reg_numb, String owner, String model) {
        super(page, limit);
        this.reg_numb = reg_numb;
        this.owner = owner;
        this.model = model;
    }

    public String getReg_numb() {
        return reg_numb;
    }

    public void setReg_numb(String reg_numb) {
        this.reg_numb = reg_numb;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
