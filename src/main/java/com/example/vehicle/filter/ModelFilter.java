package com.example.vehicle.filter;

public class ModelFilter extends FilterBase {
    private String name;

    public ModelFilter(Integer page, Integer limit, String name) {
        super(page, limit);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
