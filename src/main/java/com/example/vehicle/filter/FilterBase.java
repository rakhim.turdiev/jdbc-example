package com.example.vehicle.filter;

public class FilterBase {
    private Integer page = 1;
    private Integer limit = 20;

    public FilterBase() {
    }

    public FilterBase(Integer page, Integer limit) {
        if (page != null)
            this.page = page;
        if (limit != null)
            this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
