package com.example.vehicle.filter;

import java.time.LocalDate;

public class TravelFilter extends FilterBase {
    private String reg_numb;
    private String owner;
    private String model;
    private LocalDate from;
    private LocalDate to;

    public TravelFilter(Integer page, Integer limit, String reg_numb, String owner, String model, LocalDate from, LocalDate to) {
        super(page, limit);
        this.reg_numb = reg_numb;
        this.owner = owner;
        this.model = model;
        this.from = from;
        this.to = to;
    }

    public String getReg_numb() {
        return reg_numb;
    }

    public void setReg_numb(String reg_numb) {
        this.reg_numb = reg_numb;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }
}