package com.example.vehicle;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.sql.DriverManager;
import java.util.Objects;
import java.util.Properties;

@EnableAspectJAutoProxy
@SpringBootApplication
public class VehicleApplication implements CommandLineRunner {

    @Value(value = "${spring.datasource.url}")
    private String dbUrl;
    @Value(value = "${spring.datasource.username}")
    private String user;
    @Value(value = "${spring.datasource.password}")
    private String password;

    public static void main(String[] args) {
        SpringApplication.run(VehicleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Properties properties = new Properties();
        java.sql.Connection connection;
        connection = DriverManager.getConnection(dbUrl, user, password);

        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
        try (Liquibase liquibase = new liquibase.Liquibase("classpath:db/liquibase/liquibase-change-log.xml",
                new ClassLoaderResourceAccessor(), database)) {
            properties.forEach((key, value) -> liquibase.setChangeLogParameter(Objects.toString(key), value));
            liquibase.update(new Contexts(), new LabelExpression());
        }
    }
}