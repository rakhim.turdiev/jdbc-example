package com.example.vehicle.config;

public @interface Table {
    String name();
}
