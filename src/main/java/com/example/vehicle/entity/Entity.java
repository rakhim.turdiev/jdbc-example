package com.example.vehicle.entity;

import com.example.vehicle.entity.enums.State;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;

import static com.example.vehicle.utils.Utils.camelToSnake;

public abstract class Entity {
    private Long id;
    private State state;

    public Entity(Long id, State state) {
        this.id = id;
        this.state = state;
    }

    public Entity(State state) {
        this.state = state;
    }

    public Entity() {
    }

    public void setMap(Map map) {
        for (Field field : FieldUtils.getAllFields(this.getClass())) {
            field.setAccessible(true);
            try {
                Object value = map.get(camelToSnake(field.getName()));
                if (value == null) {
                    field.set(this, null);
                    continue;
                }
                switch (field.getType().getSimpleName()) {
                    case "LocalDate": {
                        LocalDate date = LocalDate.parse("" + value);
                        field.set(this, date);
                        break;
                    }
                    case "LocalTime": {
                        LocalTime time = LocalTime.parse("" + value);
                        field.set(this, time);
                        break;
                    }
                    default:
                        if (field.getType().isEnum()) {
                            Enum.valueOf((Class) field.getType(), "" + value);
                        } else
                            field.set(this, value);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isNew() {
        return id == null;
    }
}