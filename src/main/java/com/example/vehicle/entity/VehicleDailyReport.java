package com.example.vehicle.entity;

import com.example.vehicle.entity.enums.State;

import java.time.LocalDate;
import java.time.LocalTime;

public class VehicleDailyReport extends Entity {
    private Long vehicleId;
    private LocalDate date;
    private Integer kilometer;
    private Integer travel_count;
    private LocalTime during_time;
    private Integer fuel;
    private Integer avgSpeed;

    public VehicleDailyReport() {
        super(State.NEW);
    }

    public VehicleDailyReport(Long vehicleId, LocalDate date) {
        super(State.NEW);
        this.vehicleId = vehicleId;
        this.date = date;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getKilometer() {
        return kilometer;
    }

    public void setKilometer(Integer kilometer) {
        this.kilometer = kilometer;
    }

    public Integer getTravel_count() {
        return travel_count;
    }

    public void setTravel_count(Integer travel_count) {
        this.travel_count = travel_count;
    }

    public LocalTime getDuring_time() {
        return during_time;
    }

    public void setDuring_time(LocalTime during_time) {
        this.during_time = during_time;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    public Integer getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(Integer avgSpeed) {
        this.avgSpeed = avgSpeed;
    }
}