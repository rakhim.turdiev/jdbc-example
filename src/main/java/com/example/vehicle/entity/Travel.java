package com.example.vehicle.entity;

import com.example.vehicle.entity.enums.State;

import java.time.LocalDate;
import java.time.LocalTime;

public class Travel extends Entity {
    private Long vehicleId;
    private Integer odometrStart;
    private Integer odometrFinish;
    private String route;
    private String description;
    private LocalDate startDate;
    private LocalTime startTime;
    private LocalDate endDate;
    private LocalTime endTime;
    private Integer fuel = 0;

    public Travel() {
        super(State.NEW);
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Integer getOdometrStart() {
        return odometrStart;
    }

    public void setOdometrStart(Integer odometrStart) {
        this.odometrStart = odometrStart;
    }

    public Integer getOdometrFinish() {
        return odometrFinish;
    }

    public void setOdometrFinish(Integer odometrFinish) {
        this.odometrFinish = odometrFinish;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }
}