package com.example.vehicle.entity;

import com.example.vehicle.config.Table;
import com.example.vehicle.entity.enums.State;

@Table(name = "vehicle")
public class Vehicle extends Entity {
    private String colour;
    private Long model_id;
    private String owner;
    private String reg_numb;
    private Integer odometr = 0;

    public Vehicle() {
        super(State.NEW);
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Long getModel_id() {
        return model_id;
    }

    public void setModel_id(Long model_id) {
        this.model_id = model_id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getReg_numb() {
        return reg_numb;
    }

    public void setReg_numb(String reg_numb) {
        this.reg_numb = reg_numb;
    }

    public Integer getOdometr() {
        return odometr;
    }

    public void setOdometr(Integer odometr) {
        this.odometr = odometr;
    }
}