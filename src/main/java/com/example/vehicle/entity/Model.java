package com.example.vehicle.entity;


import com.example.vehicle.entity.enums.State;

public class Model extends Entity {
    private String name;

    public Model() {
        this.setState(State.NEW);
    }

    public Model(Long id, State state, String name) {
        super(id, state);
        this.name = name;
    }

    public Model(String name) {
        super(State.NEW);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}