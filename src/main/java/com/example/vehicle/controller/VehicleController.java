package com.example.vehicle.controller;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Vehicle;
import com.example.vehicle.filter.VehicleFilter;
import com.example.vehicle.service.VehicleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/api/vehicle")
@RestController
public class VehicleController {
    private final VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/list")
    public ResponseEntity<ListResult> list(
            @RequestParam(required = false) String reg_numb,
            @RequestParam(required = false) String owner,
            @RequestParam(required = false) String model,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit) {
        return ResponseEntity.ok(vehicleService.list(new VehicleFilter(page, limit, reg_numb, owner, model)));
    }

    @GetMapping("/items")
    public ResponseEntity<List> items(
            @RequestParam(required = false) String reg_numb,
            @RequestParam(required = false) String owner,
            @RequestParam(required = false) String model,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit
    ) {
        return ResponseEntity.ok(vehicleService.items(new VehicleFilter(page, limit, reg_numb, owner, model)));
    }

    @GetMapping("/get-by-id")
    public ResponseEntity<Map> items(@RequestParam Long id) {
        return ResponseEntity.ok(vehicleService.getById(id));
    }

    @DeleteMapping("/deleted-by-id")
    public ResponseEntity<Boolean> delete(Long id) {
        return ResponseEntity.ok(vehicleService.delete(id));
    }

    @PostMapping("/save")
    public ResponseEntity<Vehicle> save(@RequestBody Vehicle model) {
        return ResponseEntity.ok(vehicleService.save(model));
    }
}