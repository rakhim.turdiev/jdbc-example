package com.example.vehicle.controller;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.dto.TravelFinish;
import com.example.vehicle.dto.TravelStart;
import com.example.vehicle.entity.Travel;
import com.example.vehicle.filter.TravelFilter;
import com.example.vehicle.service.TravelService;
import com.example.vehicle.service.VehicleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Map;

@RequestMapping("/api/travel")
@RestController
public class TravelController {
    private final TravelService travelService;
    private final VehicleService vehicleService;

    public TravelController(TravelService travelService, VehicleService vehicleService) {
        this.travelService = travelService;
        this.vehicleService = vehicleService;
    }

    @GetMapping("/list")
    public ResponseEntity<ListResult> list(
            @RequestParam(required = false) String reg_numb,
            @RequestParam(required = false) String owner,
            @RequestParam(required = false) String model,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) LocalDate from,
            @RequestParam(required = false) LocalDate to
    ) {
        return ResponseEntity.ok(travelService.list(new TravelFilter(page, limit, reg_numb, owner, model, from, to)));
    }

    @GetMapping("/get-by-id")
    public ResponseEntity<Map> items(@RequestParam Long id) {
        return ResponseEntity.ok(travelService.getById(id));
    }

    @DeleteMapping("/deleted-by-id")
    public ResponseEntity<Boolean> delete(Long id) {
        return ResponseEntity.ok(travelService.delete(id));
    }

    @PostMapping("/change")
    public ResponseEntity<Travel> change(@RequestBody Travel model) {
        return ResponseEntity.ok(travelService.save(model));
    }

    @PostMapping("/start")
    public ResponseEntity<Travel> start(@RequestBody TravelStart travelStart) {
        Integer odometr = vehicleService.getVehicleOdometer(travelStart.getVehicleId());
        return ResponseEntity.ok(travelService.start(travelStart, odometr));
    }

    @PostMapping("/finish")
    public ResponseEntity<Travel> finish(@RequestBody TravelFinish travelFinish) {

        Travel travel = travelService.finish(travelFinish);
        vehicleService.updateOdometer(travel.getVehicleId(), travel.getOdometrFinish());

        return ResponseEntity.ok(travel);
    }
}