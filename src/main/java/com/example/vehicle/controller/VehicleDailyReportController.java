package com.example.vehicle.controller;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.service.VehicleDailyReportService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RequestMapping("/api/vehicle-daily-report")
@RestController
public class VehicleDailyReportController {
    private final VehicleDailyReportService vehicleDailyReportService;

    public VehicleDailyReportController(VehicleDailyReportService vehicleDailyReportService) {
        this.vehicleDailyReportService = vehicleDailyReportService;
    }

    @GetMapping("/list")
    public ResponseEntity<ListResult> list(@RequestParam(required = false) Long vehicleId,
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") @RequestParam(required = false) LocalDate date,
                                           @RequestParam(required = false) Integer page,
                                           @RequestParam(required = false) Integer limit) {
        return ResponseEntity.ok(vehicleDailyReportService.list(vehicleId, date, page, limit));
    }
}