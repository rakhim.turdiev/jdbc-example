package com.example.vehicle.controller;

import com.example.vehicle.dto.ListResult;
import com.example.vehicle.entity.Model;
import com.example.vehicle.filter.ModelFilter;
import com.example.vehicle.service.ModelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/model")
public class ModelController {
    private final ModelService modelService;

    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping("/list")
    public ResponseEntity<ListResult> list(@RequestParam(required = false) String name,
                                           @RequestParam(required = false) Integer page,
                                           @RequestParam(required = false) Integer limit) {
        return ResponseEntity.ok(modelService.list(new ModelFilter(page, limit, name)));
    }

    @GetMapping("/items")
    public ResponseEntity<List> items(@RequestParam(required = false) String name,
                                      @RequestParam(required = false) Integer page) {
        return ResponseEntity.ok(modelService.items(new ModelFilter(page, 10, name)));
    }

    @GetMapping("/get-by-id")
    public ResponseEntity<Map> items(@RequestParam Long id) {
        return ResponseEntity.ok(modelService.getById(id));
    }

    @PostMapping("/save")
    public ResponseEntity<Model> save(@RequestBody Model model) {
        return ResponseEntity.ok(modelService.save(model));
    }

    @DeleteMapping("/deleted-by-id")
    public ResponseEntity<Boolean> delete(@RequestParam Long id) {
        return ResponseEntity.ok(modelService.delete(id));
    }
}