package com.example.vehicle.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class TravelFinish {
    private Long travelId;
    private Integer kilometer;
    private Integer fuel;
    private LocalDate date;
    private LocalTime time;

    public Long getTravelId() {
        return travelId;
    }

    public void setTravelId(Long travelId) {
        this.travelId = travelId;
    }

    public Integer getKilometer() {
        return kilometer;
    }

    public void setKilometer(Integer kilometer) {
        this.kilometer = kilometer;
    }

    public TravelFinish(Long travelId, Integer kilometer, Integer fuel, LocalDate date, LocalTime time) {
        this.travelId = travelId;
        this.kilometer = kilometer;
        this.fuel = fuel;
        this.date = date;
        this.time = time;
    }

    public TravelFinish() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }
}