package com.example.vehicle.dto;

import com.example.vehicle.entity.Travel;

import java.time.LocalDate;
import java.time.LocalTime;

public class TravelStart {
    private Long vehicleId;
    private String route;
    private String description;
    private LocalDate date;
    private LocalTime time;

    public TravelStart() {
    }

    public TravelStart(Long vehicleId, String route, String description, LocalDate date, LocalTime time) {
        this.vehicleId = vehicleId;
        this.route = route;
        this.description = description;
        this.date = date;
        this.time = time;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Travel wrap() {
        Travel travel = new Travel();
        travel.setRoute(route);
        travel.setDescription(description);
        travel.setVehicleId(vehicleId);
        travel.setStartDate(date);
        travel.setStartTime(time);
        return travel;
    }
}