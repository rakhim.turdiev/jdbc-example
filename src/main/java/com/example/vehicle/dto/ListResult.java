package com.example.vehicle.dto;

import java.util.List;

public class ListResult {
    private final List rows;
    private final Long total;

    public ListResult(List rows, Long total) {
        this.rows = rows;
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public Long getTotal() {
        return total;
    }
}
