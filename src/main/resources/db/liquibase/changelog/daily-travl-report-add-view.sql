create or replace view dailt_travel_report as
with t as (select case
                      when start_date = end_date then (end_time - start_time)::time
                      when start_date != current_date then start_time
                      when end_date != current_date then ('24:00:00'::time - start_time)::time end during_time
                , *
           from travel
           where current_date in (start_date, end_date)),
     k as (select id,
                  extract(epoch from during_time /
                                     extract(epoch from (end_date + end_time) - (start_date + start_time))) coefficient
           from t)
select t.vehicle_id,sum((t.odometr_finish - t.odometr_start) * k.coefficient) kilometer,
       sum(t.during_time)::time during_time,
        count(t.id) travel_count,
       sum(t.fuel * k.coefficient) fuel
from t
         join k on t.id = k.id
group by t.vehicle_id;