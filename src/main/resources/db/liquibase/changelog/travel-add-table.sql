create table travel
(
    id             bigserial primary key,
    vehicle_id     bigint,
    odometr_start  integer,
    state          varchar,
    odometr_finish integer,
    route          varchar,
    description    varchar,
    start_date     date,
    start_time     time,
    end_date       date,
    end_time       time,
    fuel           integer,
    constraint add foreign key (vehicle_id) references vehicle (id)
)