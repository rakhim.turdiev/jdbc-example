create table vehicle
(
    id       bigserial primary key,
    colour   varchar,
    state    varchar,
    model_id bigint,
    odometr  integer,
    owner    varchar,
    reg_numb varchar,
    constraint add foreign key (model_id) references model (id)
);