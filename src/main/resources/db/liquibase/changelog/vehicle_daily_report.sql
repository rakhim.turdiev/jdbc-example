create table vehicle_daily_report
(
    id           bigserial primary key,
    state        varchar,
    date         date,
    vehicle_id   bigint,
    kilometer    integer,
    travel_count integer,
    during_time  time,
    fuel         integer,
    avg_speed    integer
)