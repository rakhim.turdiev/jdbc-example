<p>Purpose<br />
According to accounting rules, travel logs should be kept when a personal car is used for<br />
company needs.<br />
You need to create a backend for an application with following functionalities:<br />
● Travel log component(s), where user can:<br />
○ Add a new log entry. Each log entry consists of:<br />
■ Date<br />
■ Vehicle registration number<br />
■ Vehicle owner&rsquo;s name<br />
■ Odometer value at the beginning of a journey<br />
■ Odometer value at the end of a journey<br />
■ Route (i.e. &ldquo;Tallinn-Rakvere&rdquo;)<br />
■ Journey short description (i.e. &ldquo;buying office supplies&rdquo;)<br />
○ Edit log entry<br />
○ Delete log entry<br />
○ Generate a report based on existing logs. The report is a list of travel log entries<br />
that are grouped by date and sorted by odometer initial value. Before generating<br />
a report the user should be able to customize a report by specifying none or all of<br />
the following filters:<br />
■ Period filter: specific date range (i.e. &ldquo;01.03.2020 - 01.04.2020&rdquo;)<br />
■ Vehicle registration number filter<br />
■ Vehicle owner&rsquo;s name filter<br />
Also, the report should contain a total amount of traveled distance in the specified<br />
period.</p>

<p>Architecture constraints<br />
● The application backend should be implemented using Springboot and Java 14.<br />
● Gradle should be used for building the backend app.<br />
● Communication is via REST services<br />
● Application data should be kept in PostgreSQL.<br />
● Use of Hibernate or other ORM solutions is prohibited, Spring JdbcTemplate and plain<br />
SQL should be used.</p>

<p>● The application should use Liquibase to create all required database schemas, tables<br />
and test-data on startup.<br />
● Test data should contain at least five travel logs for at least two different vehicles.<br />
Documentation<br />
● Installation and running guide should be provided.</p>
